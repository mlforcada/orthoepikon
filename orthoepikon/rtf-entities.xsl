<?xml version="1.0" encoding="iso-8859-1" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="text" encoding="iso-8859-1"/>

<xsl:variable name="ANOT1">"{\\chcbpat"&lt;&lt;index1&lt;&lt;" \\highlight"&lt;&lt;index1&lt;&lt;" {\\cf"&lt;&lt;index2&lt;&lt;" </xsl:variable>
<xsl:variable name="ANOT2">}{\\cf"&lt;&lt;index3&lt;&lt;"\\super </xsl:variable>
<xsl:variable name="ANOT3">}}"</xsl:variable>

<!-- Anotaciones provisionales -->
<xsl:template match="aDef">
    <!-- Nombre de la anotaci�n en may�sculas -->
    <!-- Se usa para las entidades en may�sculas -->
    <xsl:variable name="nombre_mays"><xsl:value-of select="translate(../@n, 'abcdefghijklmn�opqrstuvwxyz������������', 'ABCDEFGHIJKLMN�OPQRSTUVWXYZ������������')"/></xsl:variable>
    <xsl:variable name="ort_mays"><xsl:value-of select="translate(../@orth, 'abcdefghijklmn�opqrstuvwxyz������������', 'ABCDEFGHIJKLMN�OPQRSTUVWXYZ������������')"/></xsl:variable>

<xsl:text>"&amp;ap_</xsl:text>
<xsl:value-of select="../@n"/>
<xsl:text>;"  { cout &lt;&lt; "</xsl:text>
<xsl:value-of select="../@orth"/>
<xsl:text>"; }
</xsl:text>
<xsl:text>"&amp;AP_</xsl:text>
<xsl:value-of select="$nombre_mays"/>
<xsl:text>;" { cout &lt;&lt; "</xsl:text>
<xsl:value-of select="translate(../@orth, 'abcdefghijklmn�opqrstuvwxyz������������', 'ABCDEFGHIJKLMN�OPQRSTUVWXYZ������������')"/>
<xsl:text>"; }
</xsl:text>

<xsl:text>"&amp;</xsl:text>
<xsl:value-of select="../@n"/>
<xsl:text>_</xsl:text>
<xsl:value-of select="@n"/>
<xsl:text>;"  { paleta(); cout &lt;&lt; </xsl:text>
<xsl:value-of select="$ANOT1"/>
<xsl:value-of select="../@orth"/>
<xsl:value-of select="$ANOT2"/>
<xsl:value-of select="@n"/>
<xsl:value-of select="$ANOT3"/>
<xsl:text>; }
</xsl:text>
<xsl:text>"&amp;</xsl:text>
<xsl:value-of select="$nombre_mays"/>
<xsl:text>_</xsl:text>
<xsl:value-of select="translate(@n, 'abcdefghijklmn�opqrstuvwxyz������������', 'ABCDEFGHIJKLMN�OPQRSTUVWXYZ������������')"/>
<xsl:text>;"  { paleta(); cout &lt;&lt; </xsl:text>
<xsl:value-of select="$ANOT1"/>
<xsl:value-of select="translate(../@orth, 'abcdefghijklmn�opqrstuvwxyz������������', 'ABCDEFGHIJKLMN�OPQRSTUVWXYZ������������')"/>
<xsl:value-of select="$ANOT2"/>
<xsl:value-of select="@n"/>
<xsl:value-of select="$ANOT3"/>
<xsl:text>; }
</xsl:text>

</xsl:template>


<!-- Anotaciones definitivas -->
<xsl:template match="daDef">
    <!-- Nombre de la anotaci�n en may�sculas -->
    <!-- Se usa para las entidades en may�sculas -->
    <xsl:variable name="nombre_mays"><xsl:value-of select="translate(@n, 'abcdefghijklmn�opqrstuvwxyz������������', 'ABCDEFGHIJKLMN�OPQRSTUVWXYZ������������')"/></xsl:variable>
    <xsl:variable name="ort_mays"><xsl:value-of select="translate(@orth, 'abcdefghijklmn�opqrstuvwxyz������������', 'ABCDEFGHIJKLMN�OPQRSTUVWXYZ������������')"/></xsl:variable>
<xsl:text>"&amp;</xsl:text>
<xsl:value-of select="@n"/>
<xsl:text>;"  { paleta(); cout &lt;&lt; </xsl:text>
<xsl:value-of select="$ANOT1"/>
<xsl:value-of select="@orth"/>
<xsl:value-of select="$ANOT2"/>
<xsl:value-of select="@annot"/>
<xsl:value-of select="$ANOT3"/>
<xsl:text>; }
</xsl:text>
<xsl:text>"&amp;</xsl:text>
<xsl:value-of select="$nombre_mays"/>
<xsl:text>;"  { paleta(); cout &lt;&lt; </xsl:text>
<xsl:value-of select="$ANOT1"/>
<xsl:value-of select="translate(@orth, 'abcdefghijklmn�opqrstuvwxyz������������', 'ABCDEFGHIJKLMN�OPQRSTUVWXYZ������������')"/>
<xsl:value-of select="$ANOT2"/>
<xsl:value-of select="@annot"/>
<xsl:value-of select="$ANOT3"/>
<xsl:text>; }
</xsl:text>
</xsl:template>

<xsl:template match="text()"/>

</xsl:stylesheet>
