<?xml version="1.0" encoding="iso-8859-1" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="xml" encoding="iso-8859-1" indent="no"/>

<xsl:template match="/orthoepikon">
  <dictionary>
    <xsl:copy-of select="alphabet"/>
    <sdefs>
      <xsl:apply-templates select="annotDefSec"/>
    </sdefs>
    <xsl:apply-templates select="preannotSec"/>
  </dictionary>
</xsl:template>

<xsl:template match="daDef">
  <sdef n="{@n}"/>
</xsl:template>

<xsl:template match="taDef">
  <sdef n="ap_{@n}"/>
</xsl:template>

<xsl:template match="pDefSec">
  <pardefs>
    <xsl:apply-templates/>
  </pardefs>
</xsl:template>

<xsl:template match="pDef">
  <pardef n="{@n}">
    <xsl:apply-templates/>
  </pardef>
</xsl:template>

<xsl:template match="e">
  <e><xsl:apply-templates/></e>
</xsl:template>

<xsl:template match="t">
  <p><l><xsl:apply-templates mode="fs"/></l><r><xsl:apply-templates mode="fl"/></r></p>
</xsl:template>

<xsl:template match="t/text()" mode="fs">
  <xsl:value-of select="."/>
</xsl:template>

<xsl:template match="t/text()" mode="fl">
  <xsl:value-of select="."/>
</xsl:template>

<xsl:template match="da" mode="fs">
  <xsl:value-of select="text()"/>
</xsl:template>

<xsl:template match="da" mode="fl">
  <s n="{@annot}"/>
</xsl:template>

<xsl:template match="ta" mode="fs">
  <xsl:value-of select="text()"/>
</xsl:template>

<xsl:template match="ta" mode="fl">
  <s n="ap_{@annot}"/>
</xsl:template>

<xsl:template match="p">
  <par n="{@n}"/>
</xsl:template>

<xsl:template match="dic">
  <section id="main" type="standard">
    <xsl:apply-templates/>
  </section>
</xsl:template>

<xsl:template match="text()"/>

</xsl:stylesheet>
