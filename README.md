# orthoepikon

This is an attempt to salvage the Orthoepikon project, which generated furigana-style annotations on top of HTML files showing the pronunciation of words.

It was originally hosted in [SourceForge](https://sourceforge.net/projects/orthoepikon/).

Then it was moved to [Gna!](http://home.gna.org/orthoepikon/) but Gna! is down since 2017. I will try to recover from [archives](https://www.archiveteam.org/index.php?title=Gna!/code_and_downloads#Upload_schedule).

