<?xml version="1.0" encoding="iso-8859-1" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="text" encoding="iso-8859-1"/>

<xsl:template match="text()"/>

<xsl:template name="escapar">
    <xsl:param name="cadena"/>
    <xsl:if test="string-length($cadena)>0">
        <xsl:variable name="caracter" select="substring($cadena, 1, 1)"/>
        <xsl:variable name="carmays" select="translate($caracter, 'abcdefghijklmn�opqrstuvwxyz����������', 'ABCDEFGHIJKLMN�OPQRSTUVWXYZ����������')"/>
        <xsl:variable name="carmins" select="translate($caracter, 'ABCDEFGHIJKLMN�OPQRSTUVWXYZ����������', 'abcdefghijklmn�opqrstuvwxyz����������')"/>
        <xsl:variable name="resto" select="substring-after($cadena, $caracter)"/>
        <!--<xsl:choose>
            <xsl:when test="$caracter='&quot;'"><xsl:text>\"</xsl:text></xsl:when>
            <xsl:otherwise><xsl:value-of select="$caracter"/></xsl:otherwise>
        </xsl:choose>-->
        <xsl:text>[</xsl:text>
        <xsl:value-of select="$caracter"/>
        <xsl:if test="$carmays != $caracter"><xsl:value-of select="$carmays"/></xsl:if>
        <xsl:if test="$carmins != $caracter"><xsl:value-of select="$carmins"/></xsl:if>
        <xsl:text>]</xsl:text>
        <xsl:call-template name="escapar">
            <xsl:with-param name="cadena"><xsl:value-of select="$resto"/></xsl:with-param>
        </xsl:call-template>
    </xsl:if>
</xsl:template>


<xsl:template match="orthoepikon">
    <xsl:apply-templates select="postannotSec"/>
</xsl:template>


<xsl:template match="postannotSec">
<xsl:text>%{
#include &lt;iostream&gt;
#include &lt;string&gt;
using namespace std;
%}

%option noyywrap

_CDATA   "&lt;![CDATA["([^]]|][^]]|]][^>])*"]]&gt;"
_TAGOD   "&lt;d&gt;"
_TAGCD   "&lt;/d&gt;"
_WS      ({_TAGCD})?({_CDATA}|[ \t])({_TAGOD})?

</xsl:text>
    <xsl:apply-templates select="pDefSec"/>
<xsl:text>
%%
</xsl:text>
    <xsl:apply-templates select="contexts"/>
<xsl:text>
({_TAGOD})|({_TAGCD}) {}
%%
int main(int argc, char** argv) {
    yylex();
}
</xsl:text>
</xsl:template>

<xsl:template match="pDef">
<xsl:value-of select="@n"/><xsl:text>  </xsl:text>
<xsl:apply-templates/>
<xsl:text>
</xsl:text>
</xsl:template>

<xsl:template match="pDef/e">
    <xsl:apply-templates/>
    <xsl:if test="count(following-sibling::*)>0"><xsl:text>|</xsl:text></xsl:if>
</xsl:template>

<xsl:template match="t">
    <!--<xsl:if test="string-length(text())>2 or (string-length(text())=2 and substring(text(), 1, 1)!='\')">-->
        <!--<xsl:text>"</xsl:text>-->
    <!--</xsl:if>-->
    <xsl:apply-templates/>
    <!--<xsl:call-template name="escapar">
        <xsl:with-param name="cadena"><xsl:value-of select="text()"/></xsl:with-param>
    </xsl:call-template>-->
    
<!--    <xsl:value-of select="text()"/>-->
    <!--<xsl:if test="string-length(text())>2 or (string-length(text())=2 and substring(text(), 1, 1)!='\')">-->
        <!--<xsl:text>"</xsl:text>-->
    <!--</xsl:if>-->
</xsl:template>

<xsl:template match="t/text()">
   <xsl:call-template name="escapar">
      <xsl:with-param name="cadena"><xsl:value-of select="."/></xsl:with-param>
   </xsl:call-template>
</xsl:template>

<xsl:template match="ta">
   <xsl:text>"&amp;ap_</xsl:text>
   <xsl:value-of select="@annot"/>
   <xsl:text>;"</xsl:text>
</xsl:template>

<xsl:template match="da">
   <xsl:text>"&amp;</xsl:text>
   <xsl:value-of select="@annot"/>
   <xsl:text>;"</xsl:text>
</xsl:template>

<xsl:template match="regexp">
    <xsl:text>(</xsl:text>
    <xsl:value-of select="text()"/>
    <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="p">
    <xsl:text>{</xsl:text><xsl:value-of select="@n"/><xsl:text>}</xsl:text>
</xsl:template>


<xsl:template match="contexts">
    <xsl:apply-templates select="annot[not(default)]"/>
    <xsl:apply-templates select="annot[default]"/>    
    <xsl:if test="not(annot/default)">
        <xsl:variable name="n" select="@n"/>
        <xsl:variable name="ort" select="/orthoepikon/annotDefSec/taDefSec/taDef[@n=$n]/@orth"/>
        <xsl:variable name="nombre">&amp;ap_<xsl:value-of select="@n"/>;</xsl:variable>
        <xsl:text>"</xsl:text>
        <xsl:value-of select="$nombre"/>
        <xsl:text>" { cout &lt;&lt; "</xsl:text>
        <xsl:value-of select="$ort"/>
        <xsl:text>"; }
</xsl:text>
    </xsl:if>
</xsl:template>


<xsl:template match="context">
    <xsl:apply-templates select="annotation"/>
</xsl:template>


<xsl:template match="annotation">
    <xsl:variable name="n" select="../../../@n"/>
    <xsl:variable name="nombre">&amp;ap_<xsl:value-of select="$n"/>;</xsl:variable>
    <xsl:variable name="salida">&amp;<xsl:value-of select="$n"/>_<xsl:value-of select="../../@n"/>;</xsl:variable>
    <xsl:apply-templates select="preceding-sibling::*"/>
    <xsl:text>"</xsl:text>
    <xsl:value-of select="$nombre"/>
    <xsl:text>"</xsl:text>
    <xsl:if test="following-sibling::*">
        <xsl:text>/(</xsl:text>
        <xsl:apply-templates select="following-sibling::*"></xsl:apply-templates>
        <xsl:text>)</xsl:text>
    </xsl:if>
    <xsl:text> {</xsl:text>
    <xsl:if test="preceding-sibling::*">
        <xsl:text> string s = yytext; cout &lt;&lt; s.substr(0, s.length()-</xsl:text>
        <xsl:value-of select="string-length($nombre)"/>
        <xsl:text>);</xsl:text>
    </xsl:if>
    <xsl:text> cout &lt;&lt; "</xsl:text>
    <xsl:value-of select="$salida"/>
    <xsl:text>"; }
</xsl:text>
</xsl:template>


<xsl:template match="default">
    <xsl:variable name="n" select="../../@n"/>
    <xsl:variable name="nombre">&amp;ap_<xsl:value-of select="$n"/>;</xsl:variable>
    <xsl:variable name="salida">&amp;<xsl:value-of select="$n"/>_<xsl:value-of select="../@n"/>;</xsl:variable>
    <xsl:text>"</xsl:text>
    <xsl:value-of select="$nombre"/>
    <xsl:text>" { cout &lt;&lt; "</xsl:text>
    <xsl:value-of select="$salida"/>
    <xsl:text>"; }
</xsl:text>
</xsl:template>


<xsl:template match="ws">
    <xsl:text>{_WS}</xsl:text>
</xsl:template>


</xsl:stylesheet>
