<?xml version="1.0" encoding="iso-8859-1" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="text" encoding="iso-8859-1"/>

<xsl:template match="annotDefSec">
  <xsl:text>#!/bin/bash
</xsl:text>
  <xsl:text>sed -e 's/&lt;d&gt;//g' -e 's/&lt;\/d&gt;//g' </xsl:text>
  <xsl:apply-templates/>
</xsl:template>

<!-- Anotaciones provisionales -->
<xsl:template match="aDef">
    <!-- Nombre de la anotaci�n en may�sculas -->
    <!-- Se usa para las entidades en may�sculas -->
    <xsl:variable name="nombre_mays"><xsl:value-of select="translate(../@n, 'abcdefghijklmn�opqrstuvwxyz������������', 'ABCDEFGHIJKLMN�OPQRSTUVWXYZ������������')"/></xsl:variable>
    <xsl:variable name="ort_mays"><xsl:value-of select="translate(../@orth, 'abcdefghijklmn�opqrstuvwxyz������������', 'ABCDEFGHIJKLMN�OPQRSTUVWXYZ������������')"/></xsl:variable>

<xsl:text>-e 's/&amp;ap_</xsl:text>
<xsl:value-of select="../@n"/>
<xsl:text>;/</xsl:text>
<xsl:value-of select="../@orth"/>
<xsl:text>/g' </xsl:text>
<xsl:text>-e 's/&amp;AP_</xsl:text>
<xsl:value-of select="$nombre_mays"/>
<xsl:text>;/</xsl:text>
<xsl:value-of select="translate(../@orth, 'abcdefghijklmn�opqrstuvwxyz������������', 'ABCDEFGHIJKLMN�OPQRSTUVWXYZ������������')"/>
<xsl:text>/g' </xsl:text>

<xsl:text>-e 's/&amp;</xsl:text>
<xsl:value-of select="../@n"/>
<xsl:text>_</xsl:text>
<xsl:value-of select="@n"/>
<xsl:text>;/</xsl:text>
<xsl:text>&lt;ruby&gt;&lt;rb&gt;</xsl:text>
<xsl:value-of select="../@orth"/>
<xsl:text>&lt;\/rb&gt;&lt;rp&gt;(&lt;\/rp&gt;&lt;rt&gt;</xsl:text>
<xsl:value-of select="@n"/>
<xsl:text>&lt;\/rt&gt;&lt;rp&gt;)&lt;\/rp&gt;&lt;\/ruby&gt;</xsl:text>
<xsl:text>/g' </xsl:text>
<xsl:text>-e 's/"&amp;</xsl:text>
<xsl:value-of select="$nombre_mays"/>
<xsl:text>_</xsl:text>
<xsl:value-of select="translate(@n, 'abcdefghijklmn�opqrstuvwxyz������������', 'ABCDEFGHIJKLMN�OPQRSTUVWXYZ������������')"/>
<xsl:text>;/</xsl:text>
<xsl:text>&lt;ruby&gt;&lt;rb&gt;</xsl:text>
<xsl:value-of select="translate(../@orth, 'abcdefghijklmn�opqrstuvwxyz������������', 'ABCDEFGHIJKLMN�OPQRSTUVWXYZ������������')"/>
<xsl:text>&lt;\/rb&gt;&lt;rp&gt;(&lt;\/rp&gt;&lt;rt&gt;</xsl:text>
<xsl:value-of select="@n"/>
<xsl:text>&lt;\/rt&gt;&lt;rp&gt;)&lt;\/rp&gt;&lt;\/ruby&gt;</xsl:text>
<xsl:text>/g' </xsl:text>

</xsl:template>


<!-- Anotaciones definitivas -->
<xsl:template match="daDef">
    <!-- Nombre de la anotaci�n en may�sculas -->
    <!-- Se usa para las entidades en may�sculas -->
    <xsl:variable name="nombre_mays"><xsl:value-of select="translate(@n, 'abcdefghijklmn�opqrstuvwxyz������������', 'ABCDEFGHIJKLMN�OPQRSTUVWXYZ������������')"/></xsl:variable>
    <xsl:variable name="ort_mays"><xsl:value-of select="translate(@orth, 'abcdefghijklmn�opqrstuvwxyz������������', 'ABCDEFGHIJKLMN�OPQRSTUVWXYZ������������')"/></xsl:variable>
<xsl:text>-e 's/&amp;</xsl:text>
<xsl:value-of select="@n"/>
<xsl:text>;/</xsl:text>
<xsl:text>&lt;ruby&gt;&lt;rb&gt;</xsl:text>
<xsl:value-of select="@orth"/>
<xsl:text>&lt;\/rb&gt;&lt;rp&gt;(&lt;\/rp&gt;&lt;rt&gt;</xsl:text>
<xsl:value-of select="@annot"/>
<xsl:text>&lt;\/rt&gt;&lt;rp&gt;)&lt;\/rp&gt;&lt;\/ruby&gt;</xsl:text>
<xsl:text>/g' </xsl:text>
<xsl:text>-e 's/&amp;</xsl:text>
<xsl:value-of select="$nombre_mays"/>
<xsl:text>;/</xsl:text>
<xsl:text>&lt;ruby&gt;&lt;rb&gt;</xsl:text>
<xsl:value-of select="translate(@orth, 'abcdefghijklmn�opqrstuvwxyz������������', 'ABCDEFGHIJKLMN�OPQRSTUVWXYZ������������')"/>
<xsl:text>&lt;\/rb&gt;&lt;rp&gt;(&lt;\/rp&gt;&lt;rt&gt;</xsl:text>
<xsl:value-of select="@annot"/>
<xsl:text>&lt;\/rt&gt;&lt;rp&gt;)&lt;\/rp&gt;&lt;\/ruby&gt;</xsl:text>
<xsl:text>/g' </xsl:text>
</xsl:template>

<xsl:template match="text()"/>

</xsl:stylesheet>
